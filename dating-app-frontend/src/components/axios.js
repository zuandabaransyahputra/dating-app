import axios from 'axios'

const instance = axios.create({
    baseURL: "https://dating-apps-mern.herokuapp.com/"
})

export default instance