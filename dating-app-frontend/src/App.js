import './App.css';
import DatingCards from './components/DatingCards';
import Header from './components/Header';
import SwipeButton from './components/SwipeButton';

function App() {
  return (
    <div className="App">
      <Header />
      <DatingCards />
      <SwipeButton />
    </div>
  );
}

export default App;
